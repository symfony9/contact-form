# contact-form

a small symfony project with a contact page

# launch the project

1) download the project in local
    git clone https://gitlab.com/symfony9/contact-form.git
2) Go in the src folder
    cd src
3) download project dependencies
    composer install
3) launch server in local
     symfony server:start

# url accessible

the basic url for this application is :
http://localhost:8000/contact-form

with the choice installed you can prefilter the action available in the form
http://localhost:8000/contact-form/etre-rappele
http://localhost:8000/contact-form/contacter-le-service-facturation
http://localhost:8000/contact-form/proposer-une-fonctionnalite