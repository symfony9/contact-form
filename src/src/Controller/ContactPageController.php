<?php
/*
    type : Controler
    the aim of this controler is to display a contact form to allow user to interact with dev team
*/

namespace App\Controller;

use Bundle\ContactForm\Form\ContactFormType;
use Bundle\ContactForm\Entity\Contact;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Regex;


class ContactPageController extends Controller
{
  //Main function of the contact page 
  public function index(Environment $env, Request $request)
  {
    //First we create the contact entity that will be manage by form
    $contact = new Contact();

    //we recover the action send as parameter in route and fill contact with it
    $action = $request->attributes->get('action');
    $contact->setAction($action);

    //i choose to delegate form creation in an external type to have only page code in this controller
    $form = $this->createForm(ContactFormType::class, $contact);

    //here we manage the reply of the user send as HTTP POST
    if ($request->isMethod('POST')) {
      if ($form->handleRequest($request)->isValid()) {
        $this->addFlash('success', 'Message Envoyé. Notre équipe vous remercie de votre participation.  ');
      } else {
        $this->addFlash('error', 'Nous n\'avons pas pu envoyer le formulaire il y a des erreurs dans les champs saisies. ');
      }
    }
    $form = $form->createView();
    $display = isset ($form['rappel']);
    //$display = array_key_exists('rappel', $display);
  
    //in all cases we render the page template
    $content = $env->render('contact-form.html.twig', array(
      'form' => $form,
      'display' => $display
    ));
    return new Response($content);
  }
}

