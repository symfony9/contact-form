<?php
/*
    type : Controler
    the aim of this controler is to display a contact form to allow user to interact with dev team
*/

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class HomeController
{
  public function index(Environment $env)
  {
    $content = $env->render('index.html.twig');

    return new Response($content);
  }
}

?>