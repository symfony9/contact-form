<?php
// src/Bundle/Entity/Contact.php

namespace Bundle\ContactForm\Entity;

use Symfony\Component\Validator\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="contact")
 */
class Contact
{
  protected $firstname;
  protected $name;
  protected $phone;
  protected $email;

  // contact attribute
  protected $action;
  protected $rappel;
  protected $message;


  public function setFirstname($firstname)
  {
    $this->firstname = $firstname;
  }
  public function getFirstname()
  {
    return $this->firstname;
  }

  public function setRappel($rappel)
  {
    $this->rappel = $rappel;
  }
  public function getRappel()
  {
    return $this->rappel;
  }

  public function setName($name)
  {
    $this->name = $name;
  }
  public function getName()
  {
    return $this->name;
  }

  public function setPhone($phone)
  {
    $this->phone = $phone;
  }
  public function getPhone()
  {
    return $this->phone;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }
  public function getEmail()
  {
    return $this->email;
  }

  public function setAction($action)
  {
    $this->action = $action;
  }
  public function getAction()
  {
    return $this->action;
  }

  public function setMessage($message)
  {
    $this->message = $message;
  }
  public function getMessage()
  {
    return $this->message;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
          ->add('name', TextType::class, [
              'mapped' => false,
              'constraints' => [
                  new NotBlank([
                      'message' => 'Choose a password!'
                  ])
              ]
          ]);
      ;
  }
}
