<?php

namespace Bundle\ContactForm\Form;

use Bundle\ContactForm\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length; 


use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use IT\InputMaskBundle\Form\Type\TextMaskType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class ContactFormType extends AbstractType
{
public function buildForm(FormBuilderInterface $formBuilder, array $options)
{
    $contact = new Contact;

    $formBuilder
      ->add('firstname', TextType::class, [
        'label' => 'Prénom',
        'required' => true,
        'constraints' => [new Length([
          'min' => 2,
          'max' => 100,
          'minMessage' => 'Vous devez saisir minimum 2 caractères',
          'maxMessage' => 'Vous ne pouvez dépasser 100 caractères',
          ]
          )],
        'attr' => ['required' => 'required']
        ])
      ->add('name', TextType::class, [
        'label' => 'Nom',
        'required' => true,
        'constraints' => [new Length([
          'min' => 2,
          'max' => 100,
          'minMessage' => 'Vous devez saisir minimum 2 caractères',
          'maxMessage' => 'Vous ne pouvez dépasser 100 caractères',
          ]
          )],
        'attr' => ['required' => 'required']
        ])
      ->add('phone', TextMaskType::class, [
        'label' => 'Téléphone',
        'required' => false,
        'mask' => '99.99.99.99.99',
        'attr' => [
        'placeholder' => '0_.__.__.__.__'
        ]
        ])
      ->add('email', EmailType::class)
      ->add('action', ChoiceType::class, [
        'label' => 'Vous souhaitez',
        'choices'  => [
            'Autre' => 'contactez-nous',
            'Contacter le service commercial' => 'contacter-le-service-commercial',
            'Contacter le service facturation' => 'contacter-le-service-facturation',
            'Faire une demande sur une association ou un projet solidaire' => 'faire-une-demande-sur-une-association-ou-un-projet-solidaire',
            'Proposer une fonctionnalité' => 'proposer-une-fonctionnalite',
            'Je signale un problème technique' => 'je-signale-un-probleme-technique',
            'Faire une remarque sur le site' => 'faire-une-remarque-sur-le-site',
            'être rappelé' =>'etre-rappele',
        ]])
      ->add('message', TextareaType::class,[
        'label' => 'Message',
        'required' => true,
        'constraints' => [new Length([
          'min' => 10,
          'max' => 500,
          'minMessage' => 'Vous devez saisir minimum 10 caractères',
          'maxMessage' => 'Vous ne pouvez dépasser 500 caractères',
          ]
          )],
        'attr' => ['required' => 'required']
        ])
      ->add('save', SubmitType::class, [
        'label' => 'J\'envoie mon message'
        ]);

        $formBuilder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
}

function onPreSetData(FormEvent $event) {
  $contact = $event->getData();
  $form = $event->getForm();

  if ($contact->getAction() == 'etre-rappele')
  $form->add('rappel', ChoiceType::class, [
    'label' => 'Quand souhaitez vous être rappelé ?',
    'multiple' => true, 
    'expanded' => true,
    'required' => true,
    'constraints' => array(
         new Assert\Count(array(
             'min' => 1,
             'minMessage' => "Merci de saisir au moins une disponibilité"
         ))
         ),
    'choice_attr' => ['required' => true],
    'choices'  => [
        'Dés que possible' => 1,
        'Le matin' => 2,
        'Le midi' => 3,
        'L\'aprés-midi' => 4
    ],
    'choice_attr' => function($choice, $key, $value) {
    return ['class' => 'level_'.strtolower($key)];
  }])
  ->add('phone', TextMaskType::class, [
    'label' => 'Téléphone',
    'required' => true,
    'mask' => '99.99.99.99.99',
    'attr' => [
    'placeholder' => '0_.__.__.__.__',
    'required' => 'required'
    ]
    ]);
}


public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults([
        'data_class' => Contact::class,
    ]);
}
}