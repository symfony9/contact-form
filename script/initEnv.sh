
#/bin/sh

#1) download the project in local
    git clone https://gitlab.com/symfony9/contact-form.git
#2) Go in the src folder
    cd src
#3) download project dependencies
    composer install
#3) launch server in local
     symfony server:start